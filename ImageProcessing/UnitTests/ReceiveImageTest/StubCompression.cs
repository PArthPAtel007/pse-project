﻿//-----------------------------------------------------------------------
// <author> 
//     Suman Saurav Panda
// </author>
//
// <date> 
//     17-11-2018 
// </date>
// 
// <reviewer> 
//     Sooraj Tom 
// </reviewer>
// 
// <copyright file="StubCompression.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This is a class intended to provide stub for receive image unit testing
// </summary>
//-----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;

    /// <summary>
    /// This class provides option for compression and decompression of image.
    /// </summary>
    public class StubCompression : ICompression
    {
        /// <summary>
        /// dummy funciton for testing. Not required hence not implementd
        /// </summary>
        /// <param name="bmpDict">dummy bmpDict</param>
        /// <returns>not implemented</returns>
        public string BmpDictToString(Dictionary<int, Bitmap> bmpDict)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// dummy funciton not implemented
        /// </summary>
        /// <param name="curBitmap">current dictionary dummy</param>
        /// <param name="implementDiff">dummy implement diff field</param>
        /// <returns>not implemented</returns>
        public Dictionary<int, Bitmap> Compress(Bitmap curBitmap, bool implementDiff)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// It decompresses the given Bitmap dictionary using diff technique.
        /// </summary>
        /// <param name="curBmpDict">Dictionary of int and Bitmap representing 
        /// compressed form of current Bitmap</param>
        /// <param name="implementDiff">Give true/false whether you want to implement 
        /// diff or not</param>
        /// <returns>A Bitmap of current image</returns>
        public Bitmap Decompress(Dictionary<int, Bitmap> curBmpDict, bool implementDiff)
        {
            if (curBmpDict.ContainsKey(1))
            {
                return curBmpDict[1];
            }
            else if (curBmpDict.ContainsKey(2))
            {
                throw new NotImplementedException();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// This method helps to convert a json string back to dictionady of int, bitmap
        /// </summary>
        /// <param name="bmpDictString">Json string of dictionary of int, bitmap</param>
        /// <returns>Dictionary of int, bitmap</returns>
        public Dictionary<int, Bitmap> StringToBmpDict(string bmpDictString)
        {
            var dummyDictionary = new Dictionary<int, Bitmap>();
            Bitmap bmp1 = null;
            if (bmpDictString == "true")
            {
                bmp1 = (Bitmap)Image.FromFile(@"../../../ImageProcessing/Specs/111501032Amish/1.png");
                dummyDictionary.Add(1, bmp1);
            }
            else if (bmpDictString == "ex_bmpstring")
            {
               throw new System.ArgumentException("corrupted image", "original");
            }
            else if (bmpDictString == "ex_compress")
            {
                dummyDictionary.Add(2, bmp1);
            }

            return dummyDictionary;
        }
    }
}
