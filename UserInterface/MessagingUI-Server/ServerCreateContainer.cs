﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Creates a split container for chatting and screen sharing.
// </summary>
// <copyright file="ServerCreateContainer.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Creates a split container.
        /// </summary>
        /// <param name="clientIP">The client IP<see cref="string"/></param>
        /// <returns>The split container<see cref="SplitContainer"/></returns>
        private SplitContainer CreateSplitContainer(string clientIP)
        {
            SplitContainer splitContainer = new SplitContainer();

            //// Setting properties for split container.
            splitContainer.Dock = DockStyle.Fill;
            splitContainer.Name = clientIP + "split";
            splitContainer.Orientation = Orientation.Horizontal;

            SplitterPanel topPanel = splitContainer.Panel1;
            SplitterPanel bottomPanel = splitContainer.Panel2;

            float height = (float)this.ServerChatSectionTabs.Height;
            height *= (float)0.75;
            try
            {
                splitContainer.SplitterDistance = (int)height;
            }
            catch (Exception)
            {
                splitContainer.Dispose();
            }
            splitContainer.Panel1Collapsed = true;
            
            bottomPanel.AutoScroll = true;
            bottomPanel.AutoScrollMinSize = new Size(10, 0);
            bottomPanel.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);

            TextBox timeStamp = CreateTimeStamp(clientIP);
            PictureBox screenShareImageBox = this.CreateScreenShareImageBox(clientIP, height);

            //// Add controls.
            topPanel.Controls.Add(timeStamp);
            topPanel.Controls.Add(screenShareImageBox);
            return splitContainer;
        }
    }
}