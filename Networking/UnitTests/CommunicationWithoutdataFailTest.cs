﻿// -----------------------------------------------------------------------
// <author> 
//      Rohith Reddy G
// </author>
//
// <date> 
//      28/10/2018
// </date>
// 
// <reviewer>
//      Libin, Parth, Jude
// </reviewer>
//
// <copyright file="CommunicationWithoutdataFailTest.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file contains the test case for a basic functionality all positive
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// This class executes a basic test which in which data is not subscribed 
    /// </summary>
    public class CommunicationWithoutdataFailTest : ITest
    {
        /// <summary>
        /// event to pause the thread from executing
        /// </summary>
        private static ManualResetEvent manualReset = new ManualResetEvent(false);

        /// <summary>
        /// schema module instance
        /// </summary>
        private ISchema schema = new MessageSchema();

        /// <summary>
        /// pointer to check the data status
        /// </summary>
        private bool pointerStatus = false;

        /// <summary>
        /// message to be sent 
        /// </summary>
        private string message = "hi";

        /// <summary>
        /// ip address of the machine to which data is sent 
        /// </summary>
        private IPAddress ip = IPAddress.Parse("169.254.25.29");

        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationWithoutdataFailTest" /> class.
        /// </summary>
        /// <param name="logger">Logger object used for logging during test</param>
        public CommunicationWithoutdataFailTest(ILogger logger)
        {
            this.Logger = logger;
        }

        /// <summary>
        /// Gets or sets Logger instance.
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// This function is the part of ITest interface from where the execution starts 
        /// </summary>
        /// <returns>true or false</returns>
        public bool Run()
        {
            using (Communication profCommunication = new Communication(8015, 3, 1))
            {
                using (Communication studentCommunication = new Communication(IPAddress.Parse(profCommunication.LocalIP), 8015, 3, 1))
                {
                    this.ip = IPAddress.Parse(profCommunication.LocalIP);
                    /* subscribe for data status*/
                    studentCommunication.SubscribeForDataStatus(DataType.Message, this.StudDataStatus);
                    Logger.LogInfo("Student Subscribed for Status");
                    /* message dict */
                    Dictionary<string, string> messageDict = new Dictionary<string, string>();

                    /* message added to dict */
                    messageDict.Add("message", this.message);

                    /* packet to be sent */
                    string packet = this.schema.Encode(messageDict);

                    /* subscribe for data receival */
                    studentCommunication.Send(packet, this.ip, DataType.Message);
                    Logger.LogInfo("Send Message");

                    manualReset.WaitOne();
                }       
            }
               
            if (this.pointerStatus)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This function is used to check the status of the message which is sent 
        /// </summary>
        /// <param name="mess">id of the message</param>
        /// <param name="status">status of the message</param>
        private void StudDataStatus(string mess, StatusCode status)
        {
            Logger.LogInfo("Status Notified");
            IDictionary<string, string> messageDict = this.schema.Decode(mess, false);
            if (messageDict["message"] == this.message && status == StatusCode.Success)
            {
                Logger.LogSuccess("Status for given Message is Success");
                this.pointerStatus = true;
            }

            manualReset.Set();
        }
    }
}
