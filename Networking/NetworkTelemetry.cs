﻿// -----------------------------------------------------------------------
// <author> 
//      Libin N George
// </author>
//
// <date> 
//      16-11-2018 
// </date>
// 
// <reviewer>
//      Parth
// </reviewer>
//
// <copyright file="NetworkTelemetry.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file implements class for ITelemetry.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.QualityAssurance;

    /// <summary>
    /// Class implementing ITelemetry  
    /// </summary>
    public class NetworkTelemetry : ITelemetry
    {
        /// <summary>
        /// Helper variable for easy access of Dictionary DataCapture
        /// </summary>
        private readonly string messageRequestsCount = "No of Message Requests";

        /// <summary>
        /// Helper variable for easy access of Dictionary DataCapture
        /// </summary>
        private readonly string imageRequestsCount = "No of Image Requests";

        /// <summary>
        /// Helper variable for easy access of Dictionary DataCapture
        /// </summary>
        private readonly string messagesSentCount = "No of Messages Sent";

        /// <summary>
        /// Helper variable for easy access of Dictionary DataCapture
        /// </summary>
        private readonly string imagesSentCount = "No of Images Sent";

        /// <summary>
        /// Helper variable for easy access of Dictionary DataCapture
        /// </summary>
        private readonly string averageImageSizeAll = "Average Size of Images (All)";

        /// <summary>
        /// Helper variable for easy access of Dictionary DataCapture
        /// </summary>
        private readonly string averageMsgSizeAll = "Average Size of Messages (All)";
        
        /// <summary>
        /// Helper variable for easy access of Dictionary DataCapture
        /// </summary>
        private readonly string averageSentImageSize = "Average Size of Images (Sent)";

        /// <summary>
        /// Helper variable for easy access of Dictionary DataCapture
        /// </summary>
        private readonly string averageSentMsgSize = "Average Size of Messages (Sent)";

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkTelemetry" /> class.
        /// </summary>
        public NetworkTelemetry()
        {
            this.DataCapture.Add(this.messageRequestsCount, "0");
            this.DataCapture.Add(this.imageRequestsCount, "0");
            this.DataCapture.Add(this.messagesSentCount, "0");
            this.DataCapture.Add(this.imagesSentCount, "0");
            this.DataCapture.Add(this.averageImageSizeAll, "0");
            this.DataCapture.Add(this.averageMsgSizeAll, "0");
            this.DataCapture.Add(this.averageSentImageSize, "0");
            this.DataCapture.Add(this.averageSentMsgSize, "0");
        }

        /// <summary>
        /// Gets or sets Dictionary holding Telemetry Information
        /// </summary>
        public IDictionary<string, string> DataCapture { get; set; } = new ConcurrentDictionary<string, string>();

        /// <summary>
        /// Updates Telemetry when data is received for sending.
        /// </summary>
        /// <param name="data">Data Received for sending</param>
        /// <param name="fromModule">Module from which data is received</param>
        public void DataForSendRecevied(string data, DataType fromModule)
        {
            if (string.IsNullOrEmpty(data))
            {
                return;
            }

            this.AverageUpdateBeforeSend(data.Length, fromModule);
            this.IncrementCount(fromModule, true);
        }

        /// <summary>
        /// Updates Telemetry when data is send.
        /// </summary>
        /// <param name="data">Data Send Successfully</param>
        /// <param name="fromModule">Module to which data is send</param>
        public void IncrementSuccesfullSend(string data, DataType fromModule)
        {
            if (string.IsNullOrEmpty(data))
            {
                return;
            }

            this.AverageUpdateSendSuccess(data.Length, fromModule);
            this.IncrementCount(fromModule, false);
        }

        /// <summary>
        /// Internal function for Incrementing Message count in Telemetry.
        /// </summary>
        /// <param name="fromModule">Module in which data belongs</param>
        /// <param name="beforeSend">Set to true if data is not send</param>
        private void IncrementCount(DataType fromModule, bool beforeSend)
        {
            int newCount = 0;
            switch (fromModule)
            {
                case DataType.Message:
                    if (beforeSend)
                    {
                        newCount = int.Parse(this.DataCapture[this.messageRequestsCount], CultureInfo.CurrentCulture) + 1;
                        this.DataCapture[this.messageRequestsCount] = newCount.ToString(CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        newCount = int.Parse(this.DataCapture[this.messagesSentCount], CultureInfo.CurrentCulture) + 1;
                        this.DataCapture[this.messagesSentCount] = newCount.ToString(CultureInfo.CurrentCulture);
                    }

                    break;
                case DataType.ImageSharing:
                    if (beforeSend)
                    {
                        newCount = int.Parse(this.DataCapture[this.imageRequestsCount], CultureInfo.CurrentCulture) + 1;
                        this.DataCapture[this.imageRequestsCount] = newCount.ToString(CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        newCount = int.Parse(this.DataCapture[this.imagesSentCount], CultureInfo.CurrentCulture) + 1;
                        this.DataCapture[this.imagesSentCount] = newCount.ToString(CultureInfo.CurrentCulture);
                    }

                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Internal function for updating average size of data transfer requests.
        /// </summary>
        /// <param name="length">length of message</param>
        /// <param name="fromModule">Module in which data belongs</param>
        private void AverageUpdateBeforeSend(int length, DataType fromModule)
        {
            float size = 0;
            int count = 0;
            switch (fromModule)
            {
                case DataType.Message:
                    size = float.Parse(this.DataCapture[this.averageMsgSizeAll], CultureInfo.CurrentCulture);
                    count = int.Parse(this.DataCapture[this.messageRequestsCount], CultureInfo.CurrentCulture);
                    this.DataCapture[this.averageMsgSizeAll] = (((size * count) + length) / (count + 1)).ToString(CultureInfo.CurrentCulture);
                    break;
                case DataType.ImageSharing:
                    size = float.Parse(this.DataCapture[this.averageImageSizeAll], CultureInfo.CurrentCulture);
                    count = int.Parse(this.DataCapture[this.imageRequestsCount], CultureInfo.CurrentCulture);
                    this.DataCapture[this.averageImageSizeAll] = (((size * count) + length) / (count + 1)).ToString(CultureInfo.CurrentCulture);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Internal function for updating average size of data sent successfully.
        /// </summary>
        /// <param name="length">length of message</param>
        /// <param name="fromModule">Module in which data belongs</param>
        private void AverageUpdateSendSuccess(int length, DataType fromModule)
        {
            float size = 0;
            int count = 0;
            switch (fromModule)
            {
                case DataType.Message:
                    size = float.Parse(this.DataCapture[this.averageSentMsgSize], CultureInfo.CurrentCulture);
                    count = int.Parse(this.DataCapture[this.messagesSentCount], CultureInfo.CurrentCulture);
                    this.DataCapture[this.averageSentMsgSize] = (((size * count) + length) / (count + 1)).ToString(CultureInfo.CurrentCulture);
                    break;
                case DataType.ImageSharing:
                    size = float.Parse(this.DataCapture[this.averageSentImageSize], CultureInfo.CurrentCulture);
                    count = int.Parse(this.DataCapture[this.imagesSentCount], CultureInfo.CurrentCulture);
                    this.DataCapture[this.averageSentImageSize] = (((size * count) + length) / (count + 1)).ToString(CultureInfo.CurrentCulture);
                    break;
                default:
                    break;
            }
        }
    }
}
