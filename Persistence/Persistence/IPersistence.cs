﻿//-----------------------------------------------------------------------
// <copyright file="IPersistence.cs" company="B'15, IIT Palakkad">
//      Open Source. Feel free to use the code, but don't forget to acknowledge. 
// </copyright>
// <author>
//      Prabal Vashisht
// </author>
// <review>
//      Prabal Vashisht
// </review>
//-----------------------------------------------------------------------
namespace Masti.Persistence
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// Interface for Persistence class.
    /// </summary>
    public interface IPersistence
    {
        /// <summary>
        /// Saves the message, tagged with an appropriate session ID, in the database.
        /// </summary>
        /// <param name="message">Message to be stored</param>
        /// <returns>Returns success (true) or failure (false)</returns>
        bool SaveSession(string message);

        /// <summary>
        /// Returns a list (from beginning till the end) of messages from the database.
        /// </summary>
        /// <param name="startSessionId">Beginning session ID</param>
        /// <param name="endSessionId">Ending session ID</param>
        /// <returns>List of messages</returns>
        Collection<string> RetrieveSession(int startSessionId, int endSessionId);

        /// <summary>
        /// Deletes messages (from the beginning till the end) from the database.
        /// </summary>
        /// <param name="startSessionId">Beginning session ID</param>
        /// <param name="endSessionId">Ending session ID</param>
        /// <returns>Number of sessions deleted</returns>
        int DeleteSession(int startSessionId, int endSessionId); 

        /// <summary>
        /// Returns the current session ID.
        /// </summary>
        /// <returns>Current session ID</returns>
        int GetCurrentSessionId();
    }
}
